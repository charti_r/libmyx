#include <X11/Xlib.h>
#include "myX.h"

void myXDrawFullSquareImg(myXImg *XImg, myXCoord *coord, int size, unsigned long color)
{
  myXCoord pt;

  pt.x = coord->x;
  while (pt.x < (coord->x + size))
    {
      pt.y = coord->y;
      while (pt.y < (coord->y + size))
	{
	  myXDrawPixelImg(XImg, pt.x, pt.y, color);
	  ++pt.y;
	}
      ++pt.x;
    }
}

void myXDrawEmptySquareImg(myXImg *XImg, myXCoord *coord, int size, unsigned long color)
{
  myXCoord pt;

  pt.x = coord->x;
  pt.y = coord->y;
  while (pt.x < (coord->x + size))
    {
      myXDrawPixelImg(XImg, pt.x, pt.y, color);
      ++pt.x;
    }
  while (pt.y < (coord->y + size))
    {
      myXDrawPixelImg(XImg, pt.x, pt.y, color);
      ++pt.y;
    }
  while (pt.x >= coord->x)
    {
      myXDrawPixelImg(XImg, pt.x, pt.y, color);
      --pt.x;
    }
  while (pt.y >= coord->y)
    {
      myXDrawPixelImg(XImg, pt.x, pt.y, color);
      --pt.y;
    }
}
