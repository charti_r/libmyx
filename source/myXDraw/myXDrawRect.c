#include <X11/Xlib.h>
#include "myX.h"

static void myXDrawRectCheck(myXRect *rect)
{
  if (rect->width < 0)
    {
      rect->x += rect->width;
      rect->width = -rect->width;
    }
  if (rect->height < 0)
    {
      rect->y += rect->height;
      rect->height = -rect->height;
    }
}

void myXDrawFullRectImg(myXImg *XImg, myXRect *rect, unsigned long color)
{
  myXCoord pt;

  myXDrawRectCheck(rect);
  pt.x = rect->x;
  while (pt.x < (rect->x + rect->width))
    {
      pt.y = rect->y;
      while (pt.y < (rect->y + rect->height))
	{
	  myXDrawPixelImg(XImg, pt.x, pt.y, color);
	  ++pt.y;
	}
      ++pt.x;
    }
}

void myXDrawEmptyRectImg(myXImg *XImg, myXRect *rect, unsigned long color)
{
  myXCoord pt;

  myXDrawRectCheck(rect);
  pt.x = rect->x;
  pt.y = rect->y;
  while (pt.x < (rect->x + rect->width))
    {
      myXDrawPixelImg(XImg, pt.x, pt.y, color);
      ++pt.x;
    }
  while (pt.y < (rect->y + rect->height))
    {
      myXDrawPixelImg(XImg, pt.x, pt.y, color);
      ++pt.y;
    }
  while (pt.x >= rect->x)
    {
      myXDrawPixelImg(XImg, pt.x, pt.y, color);
      --pt.x;
    }
  while (pt.y >= rect->y)
    {
      myXDrawPixelImg(XImg, pt.x, pt.y, color);
      --pt.y;
    }
}
