#include <X11/Xlib.h>
#include <stdlib.h>
#include "myX.h"

void myXDrawFullCircleImg(myXImg *XImg, myXCoord *pt, int radius, unsigned long color)
{
  int tmp;
  int i;
  int x;
  int y;

  tmp = 1 - radius;
  i = 0;
  while (i <= radius)
    {
      x = radius + pt->x;
      y = pt->y - i - 1;
      while (++y <= pt->y + i)
	myXDrawPixelImg(XImg, x, y, color);
      x = pt->x - radius;
      while (--y >= pt->y - i)
	myXDrawPixelImg(XImg, x, y, color);
      x = i + pt->x;
      y = pt->y - radius - 1;
      while (++y <= pt->y + radius)
	myXDrawPixelImg(XImg, x, y, color);
      x = pt->x - i;
      while (--y >= pt->y - radius)
	myXDrawPixelImg(XImg, x, y, color);
      if (tmp < 0)
      	tmp += (i + 2) << 1;
      else
      	tmp += (i + 2 - (--radius)) << 1;
      ++i;
    }
}

void myXDrawEmptyCircleImg(myXImg *XImg, myXCoord *pt, int radius, unsigned long color)
{
  int tmp;
  int i;

  tmp = 1 - radius;
  i = 0;
  while (i <= radius)
    {
      myXDrawPixelImg(XImg, radius + pt->x, i + pt->y, color);
      myXDrawPixelImg(XImg, radius + pt->x, -i + pt->y, color);
      myXDrawPixelImg(XImg, -radius + pt->x, i + pt->y, color);
      myXDrawPixelImg(XImg, -radius + pt->x, -i + pt->y, color);
      myXDrawPixelImg(XImg, i + pt->x, radius + pt->y, color);
      myXDrawPixelImg(XImg, i + pt->x, -radius + pt->y, color);
      myXDrawPixelImg(XImg, -i + pt->x, radius + pt->y, color);
      myXDrawPixelImg(XImg, -i + pt->x, -radius + pt->y, color);
      if (tmp < 0)
	tmp += (i + 2) << 1;
      else
	tmp += (i + 2 - (--radius)) << 1;
      ++i;
    }
}
