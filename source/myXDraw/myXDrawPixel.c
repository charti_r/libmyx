#include <X11/Xlib.h>
#include "myX.h"

void myXDrawPixelImg(myXImg *XImg, int x, int y, unsigned long color)
{
  if ((x >= 0 && x < XImg->width) && (y >= 0 && y < XImg->height))
    {
      int octet;
      int i;

      octet = (y * XImg->oLine) + (x * XImg->oPixel);
      i = -1;
      if (XImg->endian)
	while (++i < XImg->oPixel)
	  XImg->data[octet + i] = (color << ((i << 5) / XImg->oPixel)) >> 24;
      else
	while (++i < XImg->oPixel)
	  XImg->data[octet + i] = color >> ((i << 5) / XImg->oPixel);
    }
}
