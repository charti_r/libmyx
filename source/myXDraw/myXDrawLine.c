#include <X11/Xlib.h>
#include <stdlib.h>
#include "myX.h"

static void myXDrawLineImg_1(myXImg *XImg, myXCoord *pt1, myXCoord *pt2, unsigned long color)
{
  myXCoord pt;

  pt.x = pt1->x;
  while (pt.x <= pt2->x)
    {
      if (pt2->x - pt1->x)
	{
	  pt.y = pt1->y + (((pt2->y - pt1->y) * (pt.x - pt1->x)) / (pt2->x - pt1->x));
	  myXDrawPixelImg(XImg, pt.x, pt.y, color);
	}
      ++pt.x;
    }
}

static void myXDrawLineImg_2(myXImg *XImg, myXCoord *pt1, myXCoord *pt2, unsigned long color)
{
  myXCoord pt;

  pt.y = pt1->y;
  while (pt.y <= pt2->y)
    {
      if (pt2->y - pt1->y)
	{
	  pt.x = pt1->x + (((pt2->x - pt1->x) * (pt.y - pt1->y)) / (pt2->y - pt1->y));
	  myXDrawPixelImg(XImg, pt.x, pt.y, color);
	}
      ++pt.y;
    }
}

static void myXDrawLineImg_3(myXImg *XImg, myXCoord *pt1, myXCoord *pt2, unsigned long color)
{
  myXCoord pt;

  pt.y = pt2->y;
  while (pt.y <= pt1->y)
    {
      if (pt1->y - pt2->y)
	{
	  pt.x = pt2->x + (((pt1->x - pt2->x) * (pt.y - pt2->y)) / (pt1->y - pt2->y));
	  myXDrawPixelImg(XImg, pt.x, pt.y, color);
	}
      ++pt.y;
    }
}

void myXDrawLineImg(myXImg *XImg, myXCoord *pt1, myXCoord *pt2, unsigned long color)
{
  if (pt1->x > pt2->x)
    {
      myXCoord *ptc;

      ptc = pt1;
      pt1 = pt2;
      pt2 = ptc;
    }
  if ((pt2->x - pt1->x) >= abs(pt2->y - pt1->y))
    myXDrawLineImg_1(XImg, pt1, pt2, color);
  else
    {
      if (pt1->y < pt2->y)
	myXDrawLineImg_2(XImg, pt1, pt2, color);
      else
	myXDrawLineImg_3(XImg, pt1, pt2, color);
    }
}
