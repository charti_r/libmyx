#include <X11/Xlib.h>
#include "myX.h"

int myXWaitEvent(myXData *XData, myXEvent *event, int (*function)(myXEvent *, void *), void *param)
{
  XNextEvent(XData->dpy, event);
  return (function(event, param));
}

int myXPollEvent(myXData *XData, myXEvent *event, int (*function)(myXEvent *, void *), void *param)
{
  int ret;

  ret = 0;
  while (XPending(XData->dpy))
    {
      XNextEvent(XData->dpy, event);
      if ((ret = function(event, param)))
	return (ret);
    }
  return (ret);
}
