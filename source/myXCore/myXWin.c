#include <X11/Xlib.h>
#include <stdlib.h>
#include <string.h>
#include "myX.h"

myXWin *myXNewWin(myXData *XData, myXWin *parent,
		  int x, int y, int width, int height,
		  char *title)
{
  myXWin *XWin;
  XGCValues xgcv;

  if ((XWin = malloc(sizeof(*XWin))) == NULL)
    return (NULL);
  XWin->parent = (parent ? parent->win : XData->rootWin);
  XWin->win = XCreateSimpleWindow(XData->dpy, XWin->parent,
				  x, y, width, height, 0, 0, 0);
  XWin->gc = XCreateGC(XData->dpy, XWin->win,
		       GCForeground | GCBackground | GCFunction | GCPlaneMask,
		       myXGCValuesDefault(&xgcv));
  if ((XWin->title = malloc(sizeof(*title) * (strlen(title) + 1))) == NULL)
    return (NULL);
  strcpy(XWin->title, title);
  XStoreName(XData->dpy, XWin->win, title);
  XWin->width = width;
  XWin->height = height;
  XWin->isOpen = 1;
  XWin->eventMask =
    KeyPressMask | KeyReleaseMask |
    ButtonPressMask | ButtonReleaseMask | PointerMotionMask |
    ExposureMask;
  XSelectInput(XData->dpy, XWin->win, XWin->eventMask);
  XWin->next = XData->lsXWin;
  XData->lsXWin = XWin;
  XMapWindow(XData->dpy, XWin->win);
  return (XWin);
}

void myXSetEventMaskWin(myXData *XData, myXWin *XWin, long eventMask)
{
  XWin->eventMask = eventMask;
  XSelectInput(XData->dpy, XWin->win, XWin->eventMask);
}

void myXAddEventMaskWin(myXData *XData, myXWin *XWin, long eventMask)
{
  XWin->eventMask |= eventMask;
  XSelectInput(XData->dpy, XWin->win, XWin->eventMask);
}

void myXSubEventMaskWin(myXData *XData, myXWin *XWin, long eventMask)
{
  XWin->eventMask &= ~eventMask;
  XSelectInput(XData->dpy, XWin->win, XWin->eventMask);
}

myXWin *myXGetWin(myXData *XData, myXEvent *event)
{
  myXWin *XWin;

  XWin = XData->lsXWin;
  while (XWin)
    {
      if (XWin->win == event->xany.window)
	return (XWin);
      XWin = XWin->next;
    }
  return (NULL);
}

void myXClearWin(myXData *XData, myXWin *XWin)
{
  XClearWindow(XData->dpy, XWin->win);
}

void myXDisplayWin(myXData *XData, myXWin *XWin)
{
  XMapWindow(XData->dpy, XWin->win);
}

void myXCloseWin(myXData *XData, myXWin *XWin)
{
  XWin->isOpen = 0;
  XUnmapWindow(XData->dpy, XWin->win);
}

void myXDestroyWin(myXData *XData, myXWin *XWin)
{
  myXWin *dest;
  myXWin *prev;

  dest = XData->lsXWin;
  while (dest)
    {
      if (dest->parent == XWin->win)
	{
	  myXDestroyWin(XData, dest);
	  dest = XData->lsXWin;
	}
      else
	dest = dest->next;
    }
  dest = XData->lsXWin;
  prev = NULL;
  while (dest)
    {
      if (dest == XWin)
	{
	  if (prev)
	    prev->next = XWin->next;
	  else
	    XData->lsXWin = XWin->next;
	  XDestroyWindow(XData->dpy, XWin->win);
	  XFreeGC(XData->dpy, XWin->gc);
	  free(XWin->title);
	  free(XWin);
	  dest = NULL;
	}
      else
	{
	  prev = dest;
	  dest = dest->next;
	}
    }
}
