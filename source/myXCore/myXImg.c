#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <stdlib.h>
#include "myX.h"

myXImg *myXNewImg(myXData *XData, int width, int height)
{
  myXImg *XImg;
  XGCValues xgcv;

  if ((XImg = malloc(sizeof(*XImg))) == NULL)
    return (NULL);
  if ((XImg->data = malloc(sizeof(*XImg->data) * (((width * height) + 0x20) << 2))) == NULL)
    return (NULL);
  if ((XImg->img = XCreateImage(XData->dpy, XData->visual, XData->depth,
				ZPixmap, 0, XImg->data,
				width, height, 0x20, 0)) == NULL)
    return (NULL);
  XImg->pxm = XCreatePixmap(XData->dpy, XData->rootWin, width, height, XData->depth);
  XImg->gc = XCreateGC(XData->dpy, XImg->pxm,
		       GCForeground | GCBackground | GCFunction | GCPlaneMask,
		       myXGCValuesDefault(&xgcv));
  XImg->endian = XImg->img->byte_order;
  XImg->oLine = XImg->img->bytes_per_line;
  XImg->oPixel = XImg->img->bits_per_pixel >> 3;
  XImg->width = width;
  XImg->height = height;
  myXClearImg(XImg);
  return (XImg);
}

unsigned long myXGetImgPixel(myXImg *XImg, int x, int y)
{
  return (XGetPixel(XImg->img, x, y));
}

void myXClearImg(myXImg *XImg)
{
  int size;
  int i;

  size = XImg->oLine * XImg->height;
  i = -1;
  while (++i < size)
    XImg->data[i] = 0;
}

void myXFillImg(myXImg *XImg, unsigned long color)
{
  int size;
  int i;

  size = XImg->oLine * XImg->height;
  i = -1;
  if (XImg->endian)
    while (++i < size)
      XImg->data[i] = (color << (((i % XImg->oPixel) << 5) / XImg->oPixel)) >> 24;
  else
    while (++i < size)
      XImg->data[i] = color >> (((i % XImg->oPixel) << 5) / XImg->oPixel);
}

void myXImgToWin(myXData *XData, myXImg *XImg, myXWin *XWin, int x, int y)
{
  XPutImage(XData->dpy, XImg->pxm, XImg->gc, XImg->img,
  	    0, 0, 0, 0, XImg->width, XImg->height);
  XCopyArea(XData->dpy, XImg->pxm, XWin->win, XWin->gc,
	    0, 0, XImg->width, XImg->height, x, y);
}

void myXDestroyImg(myXData *XData, myXImg *XImg)
{
  XDestroyImage(XImg->img);
  XFreePixmap(XData->dpy, XImg->pxm);
  XFreeGC(XData->dpy, XImg->gc);
  free(XImg);
}
