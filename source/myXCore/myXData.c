#include <X11/Xlib.h>
#include <stdlib.h>
#include <stdio.h>
#include "myX.h"

myXData *myXInit(char *dpy)
{
  myXData *XData;

  if ((XData = malloc(sizeof(*XData))) == NULL)
    return (NULL);
  if ((XData->dpy = XOpenDisplay(dpy)) == NULL)
    {
      fprintf(stderr, "Cant reach Xserver\nDISPLAY: %s\n", dpy ? dpy : getenv("DISPLAY"));
      return (NULL);
    }
  XData->scr = DefaultScreen(XData->dpy);
  XData->rootWin = RootWindow(XData->dpy, XData->scr);
  XData->depth = DefaultDepth(XData->dpy, XData->scr);
  XData->visual = DefaultVisual(XData->dpy, XData->scr);
  XData->cmap = DefaultColormap(XData->dpy, XData->scr);
  XData->lsXWin = NULL;
  return (XData);
}

void myXDestroyData(myXData *XData)
{
  while (XData->lsXWin)
    myXDestroyWin(XData, XData->lsXWin);
  XCloseDisplay(XData->dpy);
  free(XData);
}
