#include <X11/Xlib.h>
#include "myX.h"

XGCValues *myXGCValuesDefault(XGCValues *xgcv)
{
  xgcv->function = GXcopy;
  xgcv->plane_mask = AllPlanes;
  xgcv->foreground = ~0;
  xgcv->background = 0;
  xgcv->line_width = 1;
  xgcv->line_style = LineSolid;
  xgcv->cap_style = CapButt;
  xgcv->join_style = JoinMiter;
  xgcv->fill_style = FillSolid;
  xgcv->fill_rule = EvenOddRule;
  xgcv->arc_mode = ArcPieSlice;
  /* xgcv->tile = ; */
  /* xgcv->stipple = ; */
  xgcv->ts_x_origin = 0;
  xgcv->ts_y_origin = 0;
  /* xgcv->font = ; */
  xgcv->subwindow_mode = ClipByChildren;
  xgcv->graphics_exposures = 1;
  xgcv->clip_x_origin = 0;
  xgcv->clip_y_origin = 0;
  xgcv->clip_mask = None;
  xgcv->dash_offset = 0;
  xgcv->dashes = 4;
  return (xgcv);
}
