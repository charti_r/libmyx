#ifndef MYX_H_
# define MYX_H_

# include "myXStruct.h"

/*
** X Parameters
**
** * myXInit() will initialized the X-Server conection,
**   call this function before all other MylibX functions.
**   This funtion needs as parameter a char string called display,
**   if you do not know what display is, call it with a NULL pointer.
**
** * myXDesrtoyData() will destroy all myXWin window you created,
**   and will stop properly the connection between
**   the application and the X-Server.
*/
myXData *myXInit(char *dpy);
void myXDestroyData(myXData *XData);

/*
** Window
**
** * myXNewWin() function will create a new window,
**   it returns a myXWin pointer that will be needed later.
**   This function take some parameters as:
**   - myXWin *parent -> Its window parent, call with NULL pointer if it does not have one.
**   - int x, y -> The window coordinates (may be not suported by the OS).
**   - int width, height -> The window size.
**   - char *title -> The name of the window.
**
** * myXSetEvent() / myXAddEvent() / myXSubEvent() functions set, add and substract events
**   for a given window indicated by its eventMask parameter.
**   The list of those event masks and event type is at line 150 in /usr/include/X11/X.h X-header file.
**
** * myXGetWin() function returns the myXWin pointer associated to an myXEvent.
**   Then you will be able to know wich window is affected by the event.
**
** * myXClaerWin() function clears the window. That means the window will be fully black.
**
** * myXDisplayWin() window function will map the current window on the screen.
**
** * myXCloseWin() function will just unmap the current window and its child window of the screen.
**   It will not destroy them. For this use myXDestroyWin() function.
**
** * myXDestroyWin() function will free the current window and all of its child.
**   You will not be able to access to all of these windows after.
*/
myXWin *myXNewWin(myXData *XData, myXWin *parent, int x, int y, int width, int height, char *title);
void myXSetEventMaskWin(myXData *XData, myXWin *XWin, long eventMask);
void myXAddEventMaskWin(myXData *XData, myXWin *XWin, long eventMask);
void myXSubEventMaskWin(myXData *XData, myXWin *XWin, long eventMask);
myXWin *myXGetWin(myXData *XData, myXEvent *event);
void myXClearWin(myXData *XData, myXWin *XWin);
void myXDisplayWin(myXData *XData, myXWin *XWin);
void myXCloseWin(myXData *XData, myXWin *XWin);
void myXDestroyWin(myXData *XData, myXWin *XWin);

/*
** Image
**
** * myXNewImg() function returns an allocated myXImg pointer.
**   The width and height parameters define the image size.
**
** * myXGetPixel() function returns the specified pixel from myXImg.
**
** * myXClearImg() function fills with black the myXImg.
**
** * myXFillImg() function fills with color defined in parameters the myXImg.
**
** * myXImgToWin() function prints the myXImg to the specified window.
**
** * myXDestroyImg() function frees the XImg.
*/
myXImg *myXNewImg(myXData *XData, int width, int height);
unsigned long myXGetImgPixel(myXImg *XImg, int x, int y);
void myXClearImg(myXImg *XImg);
void myXFillImg(myXImg *XImg, unsigned long color);
void myXImgToWin(myXData *XData, myXImg *XImg, myXWin *XWin, int x, int y);
void myXDestroyImg(myXData *XData, myXImg *XImg);

/*
** Event
**
** * myXWaitEvent() function managed events for you.
**   It is a blocking event function. That means it will wait for an event to continue the program.
**   It takes a pointer to an myXEvent allready allocated.
**   This function will call your function that you specified with the function pointer,
**   then you will be able to deal with the event pointer and the param pointer.
**   Your eventHook function prototype could be that: int eventHook(myXEvent *event, void *param);
**   The myXWaitEvent() will return the value that your eventHook() function returns.
**   In your eventHook() function, you will have to deal with differents types of event.
**   Events are details in /usr/include/X11/X.h X-header file at line 150.
**
** * myXPollEvent() function do the same as myXWaitEvent. But this time it is a non-blocking event function.
**   The program will continue to run, but it will use more process.
**   If your eventHook() function return an other value than 0, it will stop the event loop.
*/
int myXWaitEvent(myXData *XData, myXEvent *event, int (*function)(myXEvent *, void *), void *param);
int myXPollEvent(myXData *XData, myXEvent *event, int (*function)(myXEvent *, void *), void *param);

/*
** Drawing
**
** All drawing function will draw pixels with the color specified in parameters.
** Color details: 0xRRGGBB Section color -> R: red;
**                                          G: green;
**                                          B: blue;
**
** * myXDrawPixelImg() function put a pixel in the myXImg at the x and y coordinates.
**
** * myXDrawLineImg() function will draw line from pt1 to pt2.
**
** * myXDrawRect() funtions will draw rectangles at the rect->x and rect->y coordinates,
**   and size will be determined by the rect->width and rect->height variables.
**
** * myXDrawSquare() functions will draw squares at coordinates with the size parameters.
**
** * myXDrawCircle() functions will draw circles with
**   a center at the coordinates pt with the radius parameter.
*/
void myXDrawPixelImg(myXImg *XImg, int x, int y, unsigned long color);
void myXDrawLineImg(myXImg *XImg, myXCoord *pt1, myXCoord *pt2, unsigned long color);
void myXDrawFullRectImg(myXImg *XImg, myXRect *rect, unsigned long color);
void myXDrawEmptyRectImg(myXImg *XImg, myXRect *rect, unsigned long color);
void myXDrawFullSquareImg(myXImg *XImg, myXCoord *coord, int size, unsigned long color);
void myXDrawEmptySquareImg(myXImg *XImg, myXCoord *coord, int size, unsigned long color);
void myXDrawFullCircleImg(myXImg *XImg, myXCoord *pt, int radius, unsigned long color);
void myXDrawEmptyCircleImg(myXImg *XImg, myXCoord *pt, int radius, unsigned long color);

/*
** Graphic Context
**
** Intern Functions.
** DO NOT USE IF YOU DO NOT KNOW WHAT YOU ARE DOING !
*/
XGCValues *myXGCValuesDefault(XGCValues *xgcv);

#endif /* !MYX_H_ */
