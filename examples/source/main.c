#include <X11/Xlib.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "myX.h"

#define WD 800
#define HT 800
#define WIN_NAME "myXLib"

typedef struct	s_param
{
  myXData	*XData;
  myXWin	*XWin;
  myXImg	*XImg;
  myXCoord	pt[2];
  int		count;
}		t_param;

int eventHook(myXEvent *event, t_param *param)
{
  switch (event->type)
    {
    case KeyPress:
      if (event->xkey.keycode == 0x9)
	myXCloseWin(param->XData, myXGetWin(param->XData, event));
      else if (event->xkey.keycode == 0x1B)
	{
	  myXClearImg(param->XImg);
	  myXClearWin(param->XData, param->XWin);
	}
      else
	printf("keycode:\t0x%X\n", event->xkey.keycode);
      break;
    case ButtonPress:
      param->pt[param->count % 2].x = event->xbutton.x;
      param->pt[param->count % 2].y = event->xbutton.y;
      if (param->count)
	{
	  myXDrawLineImg(param->XImg, &(param->pt[0]), &(param->pt[1]), 0xFF00);
	  myXImgToWin(param->XData, param->XImg, param->XWin, 0, 0);
	}
      ++param->count;
      break;
    default:
      break;
    }
  return (0);
}

int main(void)
{
  t_param param;
  myXEvent event;

  if ((param.XData = myXInit(NULL)) == NULL)
    return (EXIT_FAILURE);
  if ((param.XWin = myXNewWin(param.XData, NULL, 0, 0, WD, HT, WIN_NAME)) == NULL)
    return (EXIT_FAILURE);
  if ((param.XImg = myXNewImg(param.XData, WD, HT)) == NULL)
    return (EXIT_FAILURE);

  param.count = 0;
  while (param.XWin->isOpen)
    {
      myXWaitEvent(param.XData, &event, eventHook, &param);
      myXDisplayWin(param.XData, param.XWin);
    }
  myXDestroyImg(param.XData, param.XImg);
  myXDestroyData(param.XData);
  return (EXIT_SUCCESS);
}
