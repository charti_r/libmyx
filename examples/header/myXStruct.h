#ifndef MY_X_DATA_STRUCT_H_
# define MY_X_DATA_STRUCT_H_

# include <X11/Xlib.h>

/*
** Util Struct
**
** You will have to use them from drawing.
** You can use them for your personal programs ;)
*/
typedef struct	_myXCoord
{
  int		x;
  int		y;
}		myXCoord;

typedef struct	_myXRect
{
  int		x;
  int		y;
  int		width;
  int		height;
}		myXRect;

/*
** myXStruct
**
** Intern Structure.
** DO NOT USE IF YOU DO NOT KNOW WHAT YOU ARE DOING !
*/
typedef struct		_myXImg
{
  XImage		*img;
  Pixmap		pxm;
  GC			gc;
  int			endian;
  int			oLine;
  int			oPixel;
  int			width;
  int			height;
  char			*data;
}			myXImg;

typedef struct		_myXWin
{
  Window		win;
  Window		parent;
  GC			gc;
  char			*title;
  int			width;
  int			height;
  int			isOpen;
  long			eventMask;
  struct _myXWin	*next;
}			myXWin;

typedef struct		_myXData
{
  Display		*dpy;
  Window		rootWin;
  int			scr;
  int			depth;
  Visual		*visual;
  Colormap		cmap;
  myXWin		*lsXWin;
}			myXData;

typedef XEvent myXEvent;

#endif /* !MY_X_DATA_STRUCT_H_ */
