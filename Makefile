ARCH		:= $(shell getconf LONG_BIT)

RM		= rm -f
LIB		= -lm -lX11

NAME		= libmyX.a

INCLUDE_DIR	= include

SRC_DIR		= source
CORE_DIR	= $(SRC_DIR)/myXCore
DRAW_DIR	= $(SRC_DIR)/myXDraw

SRC		= \
		$(CORE_DIR)/myXData.c \
		$(CORE_DIR)/myXWin.c \
		$(CORE_DIR)/myXImg.c \
		$(CORE_DIR)/myXEvent.c \
		$(CORE_DIR)/myXGC.c \
		$(DRAW_DIR)/myXDrawPixel.c \
		$(DRAW_DIR)/myXDrawLine.c \
		$(DRAW_DIR)/myXDrawRect.c \
		$(DRAW_DIR)/myXDrawSquare.c \
		$(DRAW_DIR)/myXDrawCircle.c

OBJ		= $(SRC:.c=.o)

CFLAGS		+= -W -Wall -pedantic -O3
CFLAGS		+= -I./$(INCLUDE_DIR)

all: $(NAME)

$(NAME): $(OBJ)
	ar rc $(NAME) $(OBJ)
	ranlib $(NAME)
	chmod +x $(NAME)

install:
	cp $(NAME) /usr/lib$(ARCH)
	cp -r $(INCLUDE_DIR)/* /usr/include

clean:
	$(RM) $(OBJ)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
